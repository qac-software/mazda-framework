package com.qaconsultants.misc;

import org.openqa.selenium.ImmutableCapabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * DriverFactory is a helper class created to facilitate the creation of WebDrivers
 *
 */
@SuppressWarnings("unused")
public class DriverFactory {
    public enum Browser {
        FIREFOX, CHROME, IE
    }

    private static String sWebDriverRoot;

    static
    {
        sWebDriverRoot = System.getProperty("qac.webdriverDirectory");
    }

    /**
     * Set the root directory where all WebDriver executables are stored
     *
     * @param _dir Root directory containing WebDriver executables
     */
    public static void setWebDriverRoot(String _dir) {
        sWebDriverRoot = _dir;
    }

    /**
     * Return a local WebDriver specified by the Browser enum.
     *
     * @param _type Browser enum value
     * @return WebDriver instance
     * @throws Exception If the WebDriver directory is not set or the executables can not be found.
     */
    public static WebDriver getLocalDriver(Browser _type) throws Exception {
        return getLocalDriver(_type, null);
    }

    /**
     * Return a local WebDriver specified by the given String representing a Browser enum value
     *
     * @param _type Browser enum value as a String
     * @return WebDriver instance
     * @throws Exception If the WebDriver directory is not set or the executables can not be found.
     */
    public static WebDriver getLocalDriver(String _type) throws Exception {
        return getLocalDriver(Browser.valueOf(_type), null);
    }

    /**
     * Return a local WebDriver specified by the Browser enum.
     * Specify a Capabilities object to pass creation information to the WebDriver
     *
     * @param _type         Browser enum value
     * @param _capabilities Capabilities for this WebDriver to use
     * @return WebDriver instance
     * @throws Exception If the WebDriver directory is not set or the executables can not be found.
     */
    public static WebDriver getLocalDriver(Browser _type, MutableCapabilities _capabilities) throws Exception {
        if (sWebDriverRoot == null)
            throw new Exception("Failed to locate WebDrivers, WebDriver path is not set for DriverFactory." +
                    "Set the qac.webdriverDirectory System property to your WebDriver directory or call setWebDriverRoot()");

        switch (_type) {
            case FIREFOX:
                return getFirefoxDriver(_capabilities);
            case CHROME:
                return getChromeDriver(_capabilities);
            case IE:
                return getIEDriver(_capabilities);
            default:
                break;
        }
        return null;
    }

    /**
     * Return a local WebDriver specified by the given String representing a Browser enum value
     * Specify a Capabilities object to pass creation information to the WebDriver
     *
     * @param _type         Browser enum value as a String
     * @param _capabilities Capabilities for this WebDriver to use
     * @return WebDriver instance
     * @throws Exception If the WebDriver directory is not set or the executables can not be found.
     */
    public static WebDriver getLocalDriver(String _type, MutableCapabilities _capabilities) throws Exception {
        return getLocalDriver(Browser.valueOf(_type.toUpperCase()), _capabilities);
    }

    /**
     * Return a remote WebDriver instance connecting to the supplied String with the supplied Capabilities
     *
     * @param _caps          Capabilities object used to determine the original state/options of the RemoteWebDriver
     * @param _connectionURL URL that this RemoteWebDriver will attempt to connect to.
     *                       This includes the SAUCE LABS / PERFECTO connection URL or a Grid URL
     * @return Instantiated RemoteWebDriver object
     * @throws MalformedURLException MalformedURLException if the connection URL is an invalid URL
     */
    public static WebDriver getRemoteDriver(MutableCapabilities _caps, String _connectionURL) throws MalformedURLException {
        return new RemoteWebDriver(new URL(_connectionURL), _caps);
    }

    private static FirefoxDriver getFirefoxDriver(MutableCapabilities _capabilities) {
        if (System.getProperty("os.name").startsWith("Windows")) {
            System.setProperty("webdriver.gecko.driver", sWebDriverRoot + "geckodriver.exe");
        } else {
            System.setProperty("webdriver.gecko.driver", sWebDriverRoot + "geckodriver");
        }

        FirefoxOptions ffopts = new FirefoxOptions();

        if (_capabilities != null) {
            //Do Firefox Specific Setup Here
        }

        return new FirefoxDriver(ffopts);
    }

    private static ChromeDriver getChromeDriver(MutableCapabilities _capabilities) {
        if (System.getProperty("os.name").startsWith("Windows")) {
            System.setProperty("webdriver.chrome.driver", sWebDriverRoot + "chromedriver.exe");
        } else {
            System.setProperty("webdriver.chrome.driver", sWebDriverRoot + "chromedriver");
        }

        ChromeOptions cropts = new ChromeOptions();

        if (_capabilities != null) {
            //Do Chrome Specific Setup Here
        }

        return new ChromeDriver(cropts);
    }

    private static InternetExplorerDriver getIEDriver(MutableCapabilities _capabilities) {
        System.setProperty("webdriver.ie.driver", sWebDriverRoot + "IEDriverServer.exe");
        InternetExplorerOptions ieopts = new InternetExplorerOptions();

        if (_capabilities != null) {
            //Do IE Specific Setup Here
        }

        return new InternetExplorerDriver(ieopts);
    }

    private static RemoteWebDriver getRemoteDriver(ImmutableCapabilities _capabilities) throws Exception {
        if (_capabilities != null)
            return new RemoteWebDriver(_capabilities);
        else
            throw new Exception("Invalid Capabilities provided to RemoteWebDriver - Was null");
    }
}