package com.qaconsultants;

import com.qaconsultants.keywords.engine.KeywordEngine;
import com.qaconsultants.keywords.engine.KeywordFactory;
import com.qaconsultants.misc.DriverFactory;
import com.qaconsultants.keywords.Keyword;
import com.qaconsultants.config.Config;
import com.qaconsultants.config.TOMLParser;
import barrypitman.junitXmlFormatter.AntXmlRunListener;
import com.qaconsultants.misc.LogListener;
import io.qameta.allure.junit4.AllureJunit4;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.util.FileUtils;
import org.junit.runner.Result;
import org.junit.runner.JUnitCore;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import tests.examples.suites.ExampleSuiteCollection;
import tests.mazda.suites.PoCSuite;

/**
 * java-generic-framework entry point
 *
 * The Java generic framework was created to facilitate the creation of boiler-plate code that may be common to
 * projects among QAC automation programmers/scripters.
 *
 * The POM.xml has been configured to execute this application in the following manners:
 *  1) Execute through command-line using [mvn exec:java]
 *  2) Compile through command-line using [mvn compile]
 *  2) Execute through the IDE using App.java as an entry point
 *
 * Available Features/Helpers include:
 *  1) An executable is available in /resources/webdrivers to download current webdrivers for all major browsers (as of November 2017)
 *  2) A configuration class as well as a custom TOML parser for it is available (See Config.java)
 *  3) A skeleton Command-Line Interface is available in CLI.java
 *  4) A driver factory is available to facilitate the creation of WebDrivers (See DriverFactory.java)
 *  5) A collection of Keyword wrappers for Selenium functionality are available (See com.qaconsultants.keywords package)
 *          (Note: This is for use with future additions such as a Keyword executor from an external file such as an Excel file)
 *  6) A custom RunListener for the JUnitCore that will display the current test(s) in the console and output it to an external file, along with
 *     any errors that occur during the execution. (See LogListener.java)
 *
 *
 *  It is not expected or even suggested that you should use each and every piece that comes with this.
 *  Just consider this a toolbox with some common functionality/tools that have been necessary in the past and may prove useful in the future.
 *
 *  The Example test currently set-up in the Main function should prove to be a decent starting point and showcases the use of a Page Object Model pattern.
 */
public class App {

    //JUnitCore responsible for running our test suites / test cases
    static JUnitCore sTestRunner;

    /*
     *
     * Entry point for the application.
     * Performs configuration + setup prior to running JUnit through the JUnitCore
     *
     * Alternatively, if you choose to use your IDE as your test-runner, you are still able to leverage the available Page Object Model or Keywords.
     * Certain set-up may be required for the usage of DriverFactory + Config prior to running-tests through the IDE.
     *
     */
    public static void main(String[] args) throws Exception {
        /*
         * Parse arguments and perform necessary functionality defined in the CLI class
         * Implement your own CLI flags + functionality in the CLI.java class
         */
        CLI _cli = new CLI();
        _cli.parseArgs(args);

        /*
         * Load in an external configuration file using our custom TOML parser
         * Internally, this reads the TOML file and assigns the values to a predefined Java class.
         *
         * The Config class MUST be loaded with a parser prior to attempting to return an instance of Config.
         * An exception is thrown otherwise.
         */
        Config.load(new TOMLParser(new File(_cli.getConfigPath())));
        setSystemInformation(Config.getInstance());

        HashMap<String, String[]> _keywordMap = new HashMap<>();
        _keywordMap.put("com.qaconsultants.keywords.navigation.Navigate", new String[]{"FORWARD"});

        KeywordFactory _factory = new KeywordFactory(DriverFactory.getLocalDriver("CHROME"));
        Keyword _keyword = _factory.build("com.qaconsultants.keywords.navigation.Navigate", new String[]{"FORWARD"});
        System.out.println(_keyword.getClass());
        return;
        //JUNIT STUFF BELOW. TESTING ABOVE
        //
        /*
         * Instantiate a JUnitCore object
         */
        sTestRunner = new JUnitCore();

        /*
         * Add a new LogListener to the JUnitCore to allow for run-time information to be logged using our custom
         * LogListener.java class.
         */
        FileUtils.mkdir(new File(System.getProperty("qac.logDirectory")), true);
        sTestRunner.addListener(new LogListener(LogManager.getLogger("LogListener"), true));

        /*
         * Add a new AntXmlRunListener to the JUnitCore to allow for JUnit XML generation
         * https://github.com/barrypitman/JUnitXmlFormatter
         */
        if(Config.getInstance().project.generateJUnitXML) {
            System.setProperty("org.schmant.task.junit4.target", System.getProperty("qac.xmlDirectory") + "junit.xml");
            FileUtils.mkdir(new File(System.getProperty("qac.xmlDirectory")), true);
            sTestRunner.addListener(new AntXmlRunListener());
        }

        /*
         * Add a new AllureJUnit4 listener to the JUnitCore to allow for Allure report generation
         */
        if(Config.getInstance().project.generateReport) {
            FileUtils.mkdir(new File(System.getProperty("qac.reportDirectory")), true);
            sTestRunner.addListener(new AllureJunit4());
        }


        /*
         * Load the JUnit Tests/Suites into the JUnitCore via an array of unknown class types.
         * TODO: Append your new test/suites here
         */
        Class<?>[] _suiteCollections = {
                PoCSuite.class
        };

        /*
         * Execute the tests and return a Result object.
         * This Result object contains information pertaining to the executed test such as the number of tests failed.
         *
         * Optional: A ParallelComputer can be added to the JUnitCore as the first parameter to run SUITES in parallel.
         */
        Result _result = sTestRunner.run(_suiteCollections);
    }

    /**
     * Use config information to set run-time information such as system properties
     *
     * @param _configuration Config object
     */
    public static void setSystemInformation(Config _configuration) {
        // Setting the ThreadName in the current Thread context will create and organize logs into thread-respective folders.
        // ThreadContext.put("ThreadName", _configuration.project.name);

        // Set the number of threads available by the Parallel runners - Otherwise calculated.
        // Note: This single pool is utilized by ParallelSuite.class + ParallelRunner.class
        // System.setProperty("maxParallelTestThreads", "6");

        // Retrieve the current time and set system properties of this applications starting run date/time
        Calendar _cal       = Calendar.getInstance();
        String   _startDate = new SimpleDateFormat("yyyy-MM-dd").format(_cal.getTime());
        String   _startTime = new SimpleDateFormat("HH-mm-ss"  ).format(_cal.getTime());

        System.setProperty("qac.startDate", _startDate);
        System.setProperty("qac.startTime", _startTime);

        // Set the system properties related to QAC output directories.
        String _outputDir =  _startDate+ "/" + _startTime + "/";
        System.setProperty("qac.logDirectory",    _configuration.pathing.output + _outputDir + "logs/");
        System.setProperty("qac.reportDirectory", _configuration.pathing.output + _outputDir + "reports/");
        System.setProperty("qac.xmlDirectory",    _configuration.pathing.output + _outputDir + "xml/");

        // Setting the webdriverDirectory property sets the DriverFactory location for WebDrivers
        // May also be used in other classes in the future, or you can use it as well.
        System.setProperty("qac.webdriverDirectory", _configuration.pathing.webdrivers);
    }
}
