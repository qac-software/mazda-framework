package com.qaconsultants;

import org.apache.commons.cli.*;

/**
 *
 * CLI is a basic shell of a possible Command Line Interface for your application.
 *
 * To append a new command-line option:
 *    1) Use the addOption method associated with the Options object in the getOptions() method.
 *    2) Parse the new option in the parseArgs() method and call the appropriate functionality.
 *
 * Note: Options are prefixed with a dash ('-') when being passed into the CLI
 *
 * Ie;
 *
 * Option: "argTest", false, "Test CLI Argument"
 * must be invoked with '-argTest', NOT just 'argTest'
 */
public class CLI {
    private String configPath = "./resources/configuration/Config.toml"; //Default Config location

    public CLI() {

    }

    /**
     * Parse arguments supplied by the Main() function.
     *
     * @param args Array of arguments
     * @throws ParseException
     */
    public void parseArgs(String[] args) throws ParseException {
        /*
         * Create a DefaultParser (Part of Apache Commons Cli)
         */
        CommandLineParser _parser = new DefaultParser();

        /*
         *
         * Parse the options supplied by the getOptions method.
         * In the case that the user supplies the application with an unexpected argument from the command-line, this WILL throw an exception.
         *
         */
        CommandLine _cmd = _parser.parse(getOptions(), args);

        /*
         * Check whether or not our parsed CommandLine object, _cmd, contains
         * the option with the hasOption(String) method.
         *
         * If it has it, we perform the functionality defined in the following block
         */
        if (_cmd.hasOption("argTest"))
            System.out.println("This is a mock command-line argument");

        /*
         * Additionally, options containing values can be retrieved using the getOptionValue(String) method
         */
        if (_cmd.hasOption("c"))
            configPath = _cmd.getOptionValue("c");
    }

    public String getConfigPath()
    {
        return configPath;
    }

    /**
     * Build and return an Options object with available CLI options
     *
     * @return Options object
     */
    private Options getOptions() {
        Options _options = new Options();

        //Append a new option WITHOUT an expected argument value
        _options.addOption("argTest", false, "Test CLI Argument");
        //Append a new option WITH an expected argument value
        _options.addOption("c", "config", true, "Configuration File Location");

        return _options;
    }
}
