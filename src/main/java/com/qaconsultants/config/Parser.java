package com.qaconsultants.config;

/**
 *
 * Abstract class for all concrete configuration parser objects
 *
 * @param <T>
 *
 */
public abstract class Parser<T> {

    /**
     *
     * Package private abstract method to prevent end-user from creating Config objects outside of the
     * singleton provided.
     *
     * @return T
     * @throws Exception if parsing fails 
     *
     */
    abstract T parse() throws Exception;
}
