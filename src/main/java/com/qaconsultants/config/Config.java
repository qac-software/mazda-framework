package com.qaconsultants.config;

import com.qaconsultants.config.model.Section;
import com.qaconsultants.utils.ReflUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Centralized location of all config information.
 *
 * Appending a new TOML Section:
 *  - Create a new internal public class extending from Section (optional extension, but recommended)
 *  - Create a public variable within that class for each configuration option
 *  - Add an instance of this new internal class to Config and instantiate it in the constructor.
 *
 *  For example:
 *
 *  Example.toml
 *  ------------
 *  [mySection]
 *  test="test"
 *  test2="test2"
 *
 *
 *  Config.java
 *  -----------
 *  public class Config
 *  {
 *      public class mySection extends Section
 *      {
 *          public String test;
 *          public String test2;
 *      }
 *      public mySection section;
 *      ...
 *  }
 *
 *  With this setup, the TOML parser will automatically assign the variables with their appropriate values
 *  from the external configuration file.
 *
 */
public class Config
{
    //Contains all data under the [project] header in the TOML file
    public class Project extends Section {
        public String name = "Default Project";
        public boolean generateJUnitXML = false;
        public boolean generateReport = false;
    }

    //Contains all data under the [pathing] header in the TOML file
    public class Pathing extends Section {
        public String webdrivers = "./resources/webdrivers/";
        public String output     = "./output/";
    }


    public Project                    project;
    public Pathing                    pathing;
    public Map<String, Object>        capabilities;

    private static Config mInstance;

    /**
     *
     * Constructor
     *
     * Initialize all sections to their defaults values
     *
     */
    Config() {
        project      = new Project();
        pathing      = new Pathing();
        capabilities = new HashMap<>();
    }

    /**
     * Return the internal instance of Config
     *
     * @return Instantiated mInstance
     * @throws Exception if the instance hasn't been set through load()
     */
    public static Config getInstance() throws Exception {
        if(mInstance == null)
            throw new Exception("Failed to return instance of Config. Please call load() with a parser object)");
        return mInstance;
    }

    /**
     *
     * Using an Parser interface, parse and set the internal Config object
     *
     * @param _parser Concrete Parser object
     * @throws Exception if unable to parse
     *
     */
    public static void load(Parser<Config> _parser) throws Exception {
        mInstance = _parser.parse();
    }

    /**
     *
     * Loop over the fields within Config and determine whether or not they contain a given field.
     * If they do, assign them the supplied parameter [_value]
     *
     * @param _field The field to search for in the section
     * @param _value The value to associate with the field if found
     *
     */
    public void setSectionField(String _field, Object _value) {
        try {
            Section _section;
            Field[] _sections = this.getClass().getFields();
            for (Field field : _sections) {
                _section = (Section) field.get(this);
                if (_section.set(_field, _value))
                    break;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Directly attempt to access a Config Field with the name supplied by parameter [_section]
     * If we find the section, attempt to find and set a field to the supplied value.
     *
     * @param _section The section to find in this class
     * @param _field   The field to search for in the section
     * @param _value   The value to associate with the field if found
     *
     */
    public void setSectionField(String _section, String _field, Object _value) {
        if (ReflUtils.doesObjectContainField(this, _section)) {
            try {
                Field   sect    = ReflUtils.getObjectField(this, _section);
                Section section = (Section) sect.get(this);
                section.set(_field, _value);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
