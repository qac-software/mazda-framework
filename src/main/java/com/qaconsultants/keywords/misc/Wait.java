package com.qaconsultants.keywords.misc;

import com.qaconsultants.keywords.StandardKeyword;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.function.Function;


/**
 *
 * Concrete implementation of Keyword responsible for waiting
 * until an element is one of the following:
 * - Enabled
 * - Visible
 * - Selected
 *
 */
//TODO REQUIRES TESTING
public class Wait extends StandardKeyword<Function<WebDriver, Boolean>> {
    protected WebDriverWait mWait;

    /**
     * Constructor
     *
     * @param _driver WebDriver instance
     */
    public Wait(WebDriver _driver) {
        mWait = new WebDriverWait(_driver, 10);
    }

    @Override
    public boolean Execute(Function<WebDriver, Boolean> _strategy, String... _arguments) throws Exception {
        try {
           mWait.until(_strategy);
           return true;
        }
        catch (Exception e) {
            mLogger.error("An error has occurred in the Wait keyword", e);
            throw e;
        }
    }

    //Wait Methods
    public static Function<WebDriver, Boolean> waitForEnabledStrategy(WebElement _element){
        return (WebDriver d) -> _element.isEnabled();
    }

    public static Function<WebDriver, Boolean> waitForSelectedStrategy(WebElement _element){
        return (WebDriver d) -> _element.isSelected();
    }

    public static Function<WebDriver, Boolean> waitForVisibleStrategy(WebElement _element){
        return (WebDriver d) -> _element.isDisplayed();
    }

    public static Function<WebDriver, Boolean> waitForTimeStrategy(int _time){
        long _currentTime = System.currentTimeMillis();
        return (WebDriver d) -> System.currentTimeMillis() > (_currentTime + (_time * 1000));
    }
}
