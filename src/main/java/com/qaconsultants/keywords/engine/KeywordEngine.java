package com.qaconsultants.keywords.engine;

import com.qaconsultants.keywords.engine.KeywordFactory;
import com.qaconsultants.keywords.Keyword;
import org.openqa.selenium.WebDriver;

import java.util.Map;

/**
 * The KeywordEngine facilitates the connection of external files to internal keywords.
 */
public class KeywordEngine {

    private WebDriver mWebDriver;

    
    /**
     * Constructor
     */
    public KeywordEngine(WebDriver _webDriver, KeywordFactory _factory){
        mWebDriver = _webDriver;
    }

    /**
     * Run the given KeywordMap sequentially
     * @param _map KeywordMap
     * @throws Exception if an error occurs while parsing
     */
    public void runKeywordMap(Map<String, String[]> _map) throws Exception {
        try {
            for (Map.Entry<String, String[]> key : _map.entrySet()) {
                parseKeyword(key.getKey()).Execute(mWebDriver, key.getValue());
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private Keyword parseKeyword(String _keyword){
        try{

        } catch (Exception e){

        }
        return null;
    }

}
