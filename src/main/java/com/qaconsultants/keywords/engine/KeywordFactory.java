package com.qaconsultants.keywords.engine;

import java.util.Arrays;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import com.qaconsultants.keywords.Keyword;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
// TODO - Locate a class under the package com.qaconsultants.keywords with the matching string name
// TODO - Determine the arguments necessary to instantiate this object (Done on the fly - Reflection)
// TODO -

// RULES
//      - Classes that accept a WebElement as the initial argument will supply the selector as the initial (first) argument
//      - Classes that accept a WebDriver as the initial argument will have all arguments passed through
//      - Classes accepting predicates require hard-coded alternatives inside this factory to be produced.
//      -
public class KeywordFactory {
  
  private WebDriver mWebDriver;

  public KeywordFactory(WebDriver _driver){
    mWebDriver = _driver;
  }

  public Keyword build(String _className, String[] _arguments) throws Exception {
    Class       _class   = ClassLoader.getSystemClassLoader().loadClass(_className);
    Class[]     _types   = _class.getDeclaredConstructors()[0].getParameterTypes();
    Keyword     _keyword = null;

    if(_types.length > 0){
      _keyword = (Keyword) _class.getDeclaredConstructor(_types).newInstance(_arguments);
    }
    return _keyword;
  }

  private By getSelectorStrategy(String _selector){
    String[] _split = _selector.split("::");
    switch(_split[0]){
      case "xpath":
        return By.xpath(_split[1]);
      case "css":
        return By.cssSelector(_split[1]);
      case "name":
        return By.name(_split[1]);
      case "id":
        return By.id(_split[1]);
    }
    return null;
  } 


}
