package com.qaconsultants.utils;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class LoginUtils {
    public static URL getLoginURL(String username, String password, String hostAddress) throws MalformedURLException, UnsupportedEncodingException {
        return new URL(
                "https://" +
                        URLEncoder.encode(username, "UTF-8") + ":" +
                        URLEncoder.encode(password, "UTF-8") + "@" +
                        hostAddress
        );
    }
}
