package tests.mazda;

import com.qaconsultants.junit.QACTest;
import com.qaconsultants.misc.DriverFactory;
import io.qameta.allure.*;
import org.apache.logging.log4j.LogManager;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import tests.mazda.pages.CarConfigPage;
import tests.mazda.pages.LandingPage;

import java.util.Map;
import java.util.logging.Logger;

public class TestOne extends QACTest {
    private WebDriver mWebDriver;

    @Before
    public void setUp() throws Exception {
        mWebDriver = DriverFactory.getLocalDriver("IE");
        mWebDriver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        mWebDriver.quit();
    }

    @Test
    @Description("Configure a Mazda 3")
    public void ConfigureMazda3() throws Exception {
        CarConfigPage _cfgPage = (new LandingPage(mWebDriver))
                .navigate()
                .selectCar(1)
                .selectModelOption(1);

        // colors
        // select fourth color index here because it will auto select a package;
        // a later step depends on the package being selected, so make sure it's done
        _cfgPage.selectFourthColor();

        // wheels
        _cfgPage
                .selectSecondWheelSize()
                .selectThirdWheelsOption();

        // accessories
        _cfgPage
                .expandAccessoriesSection()
                .selectThirdAccessory();

        // payments
        _cfgPage.expandPaymentsSection();

        // payments > trade in
        _cfgPage
                .updateTradeinAmount("800")
                .updateDownPaymentAmount("1200")
                .toggleIncludeTaxes();

        // FINANCING OPTIONS
        //TODO externalize finance options
        _cfgPage.selectFinance60Mo();

        // LEASING OPTIONS
        _cfgPage
                .selectLeaseOpts42Months()
                .selectLeaseOptsLowKmLease();

        // totals
        Map financeTotals = _cfgPage.getFinanceTotals();
        Map leaseTotals   = _cfgPage.getLeaseTotals();
        Map cashTotals    = _cfgPage.getCashTotals();


        System.out.println("\n\nMazda 3 Finance Totals");
        financeTotals.forEach((k, v) ->
                System.out.println(k.toString() + ": " + v.toString()));

        System.out.println("\n\nMazda 3 Leasing Totals");
        leaseTotals.forEach((k, v) ->
                System.out.println(k.toString() + ": " + v.toString()));

        System.out.println("\n\nMazda 3 Cash Totals");
        cashTotals.forEach((k, v) ->
                System.out.println(k.toString() + ": " + v.toString()));
    }

    @Test
    @Description("Configure a Mazda 6")
    public void ConfigureMazda6() throws Exception {
        CarConfigPage _cfgPage = (new LandingPage(mWebDriver))
                .navigate()
                .selectCar(3)
                .selectModelOption(1);

        // colors
        //TODO externalize color options
        _cfgPage.selectFirstColor();

        // wheels
        _cfgPage
                //TODO externalize wheel size
                //TODO externalize wheel option
                .selectSecondWheelSize()
                .selectThirdWheelsOption();

        // accessories
        _cfgPage
                .expandAccessoriesSection()
                //TODO externalize accessory options
                .selectFirstAccessory();

        // payments
        _cfgPage.expandPaymentsSection();

        // payments > trade in
        _cfgPage
                .updateTradeinAmount("0")
                .updateDownPaymentAmount("0");

        // FINANCING OPTIONS
        // commented out: use default financing option
        // _cfgPage.selectFinance60Mo();

        // LEASING OPTIONS
        _cfgPage
                // commented out: use default lease option
                // .selectLeaseOpts42Months()
                .selectLeaseOptsStdKmLease();

        // totals
        Map financeTotals = _cfgPage.getFinanceTotals();
        Map leaseTotals   = _cfgPage.getLeaseTotals();
        Map cashTotals    = _cfgPage.getCashTotals();


        System.out.println("\nMazda 6 Finance Totals");
        financeTotals.forEach((k, v) ->
                System.out.println(k.toString() + ": " + v.toString()));

        System.out.println("\nMazda 6 Leasing Totals");
        leaseTotals.forEach((k, v) ->
                System.out.println(k.toString() + ": " + v.toString()));

        System.out.println("\nMazda 6 Cash Totals");
        cashTotals.forEach((k, v) ->
                System.out.println(k.toString() + ": " + v.toString()));
    }
}
