package tests.mazda.pages;

import com.qaconsultants.pom.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.HashMap;
import java.util.Map;

//TODO Allow options to be selected by their inner text.
//  Get the parent object. Search for elements containing the text. return that element
public class CarConfigPage extends Page<CarConfigPage> {
    //
    // colors
    //
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]")
    private WebElement firstColorOption;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]")
    private WebElement secondColorOption;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[1]/div[1]/div[1]/div[3]")
    private WebElement thirdColorOption;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[1]/div[1]/div[1]/div[4]")
    private WebElement fourthColorOption;

    //
    // wheels
    //
    // NOTE: currently we're only testing mazda 3 and mazda 6; both have 3 wheel size options, so we can use these xpaths
    // and not worry about indexing being wrong
    //
    // sizes
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[2]/h2/div[2]/div[3]/div/div[1]")
    private WebElement firstWheelSize;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[2]/h2/div[2]/div[2]/div/div[1]")
    private WebElement secondWheelSize;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[2]/h2/div[2]/div[1]/div/div[1]")
    private WebElement thirdWheelSize;

    // options
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[2]/div[1]/div[3]/div/div/div[1]/div/div/div[2]/div[1]")
    private WebElement firstWheelsOption;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[2]/div[1]/div[3]/div/div/div[2]/div/div/div[2]/div[1]")
    private WebElement secondWheelsOption;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[2]/div[1]/div[3]/div/div/div[3]/div/div/div[2]/div[1]")
    private WebElement thirdWheelsOption;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[1]/div/div[2]/div[1]/div[2]/div[1]/div[3]/div/div/div[4]/div/div/div[2]/div[1]")
    private WebElement fourthWheelsOption;

    //
    // accessories
    //

    // section expand/collapse
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[2]/div/div[1]")
    private WebElement accessoriesSectionExpandBtn;

    // options
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[2]/div/div[2]/div[3]/div[1]/div[1]/div/div[1]/div[1]/img")
    private WebElement accessoriesFirstOption;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[2]/div/div[2]/div[3]/div[1]/div[2]/div/div[1]/div[1]/img")
    private WebElement accessoriesSecondOption;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[2]/div/div[2]/div[3]/div[1]/div[3]/div/div[1]/div[1]/img")
    private WebElement accessoriesThirdOption;


    //
    // payments
    //

    // section expand/collapse
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[1]")
    private WebElement paymentsSectionExpandBtn;

    @FindBy(id = "step3tradin")
    private WebElement tradeInAmtInput;

    @FindBy(id = "step3downpayment")
    private WebElement downPaymentInput;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[2]/div[2]/div/div[1]/div/div/div/div[1]")
    private WebElement mazdaGraduatePrgrmRewardsCheckBox;


    // tradein / rewards section summary total number
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[2]/div[3]/div/div[2]/span[2]/span")
    private WebElement tradeInRewardsTotalNum;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[3]/div/div/div/div[1]")
    private WebElement includeTaxesCheckBox;

    // finance options
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[1]/div[1]/div[6]/div[1]/div/div[1]")
    private WebElement financeOpts60month;

    // finance totals
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[1]/div[4]/div[1]/div[1]/div[2]")
    private WebElement financeTotalBalanceToFinance;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[1]/div[4]/div[1]/div[2]/div[2]")
    private WebElement financeTotalCostOfBorrowing;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[1]/div[4]/div[2]/div[1]/div/p[2]")
    private WebElement financeTotalWeekly;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[1]/div[4]/div[2]/div[2]/div/p[2]")
    private WebElement financeTotalBiWeekly;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[1]/div[4]/div[2]/div[3]/div/p[2]")
    private WebElement financeTotalMonthly;
    // NOTE: haven't expanded "view more" section, only using the main numbers shown


    // lease options
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[2]/div[1]/div[3]/div[1]/div/div[1]")
    private WebElement leaseOpts42Months;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[2]/div[1]/div[7]/div/div[1]/div[1]")
    private WebElement leaseOptsLowKmLease;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[2]/div[1]/div[7]/div/div[2]/div[1]")
    private WebElement leaseOptsStdKmLease;


    // lease totals
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[2]/div[4]/div[1]/div[1]/div[2]")
    private WebElement leaseTotalBalanceToLease;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[2]/div[4]/div[1]/div[2]/div[2]")
    private WebElement leaseTotalTotalPayable;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[2]/div[4]/div[2]/div[1]/div/p[2]")
    private WebElement leaseTotalBiWeekly;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[2]/div[4]/div[2]/div[2]/div/p[2]")
    private WebElement leaseTotalMonthly;
    // NOTE: haven't expanded "view more" section, only using the main numbers shown


    // cash totals
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[1]/div[2]")
    private WebElement cashTotalMsrp;

    // CAREFUL: this [2] means this accessor only works when a value is provided to the "trade in" field
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[2]/div[2]")
    private WebElement cashTotalTradeIn;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[3]/div[2]")
    private WebElement cashTotalMazdaRetailCash;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[4]/div[2]")
    private WebElement cashTotalOptions;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[5]/div[2]")
    private WebElement cashTotalAccessories;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[6]/div[2]")
    private WebElement cashTotalFreight;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[7]/div[2]")
    private WebElement cashTotalAirConTax;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[8]/div[2]")
    private WebElement cashTotalOMVICFee;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[9]/div[2]")
    private WebElement cashTotalTireStwFee;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[10]/div[2]")
    private WebElement cashTotalOilFilterFee;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[11]/div[2]")
    private WebElement cashTotalSubTotal;

    // CAREFUL: this [2] means this accessor only works when a value is provided to the "trade in" field
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[13]/div[2]")
    private WebElement cashTotalDownPayment;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[14]/div[2]/span")
    private WebElement cashTotalRewards;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div[3]/div[3]/div/div[2]/div[4]/div[3]/div[1]/div[14]/div[2]/span")
    private WebElement cashTotalPurchasePrice;



    //
    //
    //
    //
    //



    public CarConfigPage(WebDriver _driver) {
        super(_driver);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getURL() {
        return null;
    }

    //
    // color options
    //

    public CarConfigPage selectFirstColor() throws Exception {
        clickElement(firstColorOption);
        return this;
    }

    public CarConfigPage selectSecondColor() throws Exception {
        clickElement(secondColorOption);
        return this;
    }

    public CarConfigPage selectThirdColor() throws Exception {
        clickElement(thirdColorOption);
        return this;

    }

    public CarConfigPage selectFourthColor() throws Exception {
        clickElement(fourthColorOption);
        return this;
    }


    //
    // wheel sizes
    //

    public CarConfigPage selectFirstWheelSize() throws Exception {
        clickElement(firstWheelSize);
        return this;
    }

    public CarConfigPage selectSecondWheelSize() throws Exception {
        clickElement(secondWheelSize);
        return this;
    }

    public CarConfigPage selectThirdWheelSize() throws Exception {
        clickElement(thirdWheelSize);
        return this;
    }

    //
    // wheel options
    //
    public CarConfigPage selectFirstWheelsOption() throws Exception {
        clickElement(firstWheelsOption);
        return this;
    }

    public CarConfigPage selectSecondWheelsOption() throws Exception {
        clickElement(secondWheelsOption);
        return this;
    }

    public CarConfigPage selectThirdWheelsOption() throws Exception {
        clickElement(thirdWheelsOption);
        return this;
    }

    public CarConfigPage selectFourthWheelsOption() throws Exception {
        clickElement(fourthWheelsOption);
        return this;
    }

    //
    // accessories
    //

    public CarConfigPage expandAccessoriesSection() throws Exception {
        clickElement(accessoriesSectionExpandBtn);
        return this;
    }

    public CarConfigPage selectFirstAccessory() throws Exception {
        clickElement(accessoriesFirstOption);
        return this;
    }

    public CarConfigPage selectSecondAccessory() throws Exception {
        clickElement(accessoriesSecondOption);
        return this;
    }

    public CarConfigPage selectThirdAccessory() throws Exception {
        clickElement(accessoriesThirdOption);
        return this;
    }

    //
    // payments
    //

    public CarConfigPage expandPaymentsSection() throws Exception {
        clickElement(paymentsSectionExpandBtn);
        return this;
    }

    // trade in and down payment

    public CarConfigPage updateTradeinAmount(String amount) throws Exception {
        inputText(tradeInAmtInput, amount);
        return this;
    }

    public CarConfigPage updateDownPaymentAmount(String amount) throws Exception {
        inputText(downPaymentInput, amount);
        return this;
    }

    // taxes

    public CarConfigPage toggleIncludeTaxes() throws Exception {
        clickElement(includeTaxesCheckBox);
        return this;
    }

    // finance options

    public CarConfigPage selectFinance60Mo() throws Exception {
        clickElement(financeOpts60month);
        return this;
    }

    // lease options

    public CarConfigPage selectLeaseOpts42Months() throws Exception {
        clickElement(leaseOpts42Months);
        return this;
    }

    public CarConfigPage selectLeaseOptsLowKmLease() throws Exception {
        clickElement(leaseOptsLowKmLease);
        return this;
    }

    public CarConfigPage selectLeaseOptsStdKmLease() throws Exception {
        clickElement(leaseOptsStdKmLease);
        return this;
    }

    // temp
    private Float cleanFloat(WebElement el) throws Exception {
        return Float.parseFloat(
                el.getText()
                .replace("$", "")
                .replace(",", "")
        );
    }

    public Map<String, Float> getFinanceTotals() throws Exception {
        Map<String, Float> _map = new HashMap<>();
        _map.put("BalanceToFinance", cleanFloat(financeTotalBalanceToFinance));
        _map.put("CostOfBorrowing", cleanFloat(financeTotalCostOfBorrowing));
        _map.put("WeeklyAmt", cleanFloat(financeTotalWeekly));
        _map.put("BiWeeklyAmt", cleanFloat(financeTotalBiWeekly));
        _map.put("MonthlyAmt", cleanFloat(financeTotalMonthly));
        return _map;
    }


    public Map<String, Float> getLeaseTotals() throws Exception {
        Map<String, Float> _map = new HashMap<>();
        _map.put("BalanceToLease", cleanFloat(leaseTotalBalanceToLease));
        _map.put("TotalPayable", cleanFloat(leaseTotalTotalPayable));
        _map.put("BiWeeklyAmt", cleanFloat(leaseTotalBiWeekly));
        _map.put("MonthlyAmt", cleanFloat(leaseTotalMonthly));
        return _map;
    }

    public Map<String, Float> getCashTotals() throws Exception {
        Map<String, Float> _map = new HashMap<>();
        _map.put("CashTotalMsrp", cleanFloat(cashTotalMsrp));
        _map.put("CashTotalTradeIn", cleanFloat(cashTotalTradeIn));
        _map.put("CashTotalMazdaRetailCash", cleanFloat(cashTotalMazdaRetailCash));
        _map.put("CashTotalOptions", cleanFloat(cashTotalOptions));
        _map.put("CashTotalAccessories", cleanFloat(cashTotalAccessories));
        _map.put("CashTotalFreight", cleanFloat(cashTotalFreight));
        _map.put("CashTotalAirConTax", cleanFloat(cashTotalAirConTax));
        _map.put("CashTotalOMVICFee", cleanFloat(cashTotalOMVICFee));
        _map.put("CashTotalTireStwFee", cleanFloat(cashTotalTireStwFee));
        _map.put("CashTotalOilFilterFee", cleanFloat(cashTotalOilFilterFee));
        _map.put("CashTotalSubTotal", cleanFloat(cashTotalSubTotal));
        _map.put("CashTotalDownPayment", cleanFloat(cashTotalDownPayment));
        _map.put("CashTotalRewards", cleanFloat(cashTotalRewards));
        _map.put("CashTotalPurchasePrice", cleanFloat(cashTotalPurchasePrice));
        return _map;
    }
}
