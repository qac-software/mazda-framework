package tests.mazda.pages;

import com.qaconsultants.pom.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LandingPage extends Page<LandingPage> {
    private final String url = "https://www.mazda.ca/en/shopping-tools/build-price/#/ON";

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div/div[1]/div[1]/div/div[2]/div[2]")
    private WebElement selectMazda3;

    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div/div/div[3]/div[1]/div/div[2]/div[2]")
    private WebElement selectMazda6;

    public LandingPage(WebDriver _driver) throws Exception {
        super(_driver);
    }

    @Override
    public String getTitle() {
        return mWebDriver.getTitle();
    }

    @Override
    public String getURL() {
        return url;
    }

    public CarSelectPage selectCar(int _index) throws Exception {
        String _location = String.format("/html/body/div[2]/div/div/section/div[6]/div/div/div/div[%d]/div[1]/div/div[2]/div[2]",
                                         _index);
        clickElement(mWebDriver.findElement(By.xpath(_location)));
        waitForTime(2);
        return new CarSelectPage(mWebDriver);
    }

    public CarSelectPage selectMazda3() throws Exception {
        clickElement(selectMazda3);
        waitForTime(2);
        return new CarSelectPage(mWebDriver);
    }

    public CarSelectPage selectMazda6() throws Exception {
        clickElement(selectMazda6);
        waitForTime(2);
        return new CarSelectPage(mWebDriver);
    }
}
