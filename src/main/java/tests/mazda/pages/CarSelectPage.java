package tests.mazda.pages;

import com.qaconsultants.pom.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CarSelectPage extends Page<CarSelectPage> {
    @FindBy(xpath = "/html/body/div[2]/div/div/section/div[6]/div/div[1]/div/div[2]/div[1]/div/div[5]")
    private WebElement firstCarOption;

    /**
     * Constructor
     *
     * @param _driver WebDriver instance
     */
    public CarSelectPage(WebDriver _driver) {
        super(_driver);
    }

    @Override
    public String getTitle() {
        return mWebDriver.getTitle();
    }

    @Override
    public String getURL() {
        return mWebDriver.getCurrentUrl();
    }

    public CarConfigPage selectModelOption(int _index) throws Exception {
        String _locator = String.format("/html/body/div[2]/div/div/section/div[6]/div/div[1]/div/div[2]/div[%d]/div/div[5]",
                                        _index);
        clickElement(mWebDriver.findElement(By.xpath(_locator)));
        return new CarConfigPage(mWebDriver);
    }

    public CarConfigPage selectFirstCarOption() throws Exception {
        clickElement(firstCarOption);
        return new CarConfigPage(mWebDriver);
    }
}
