package tests.mazda.suites;

import com.googlecode.junittoolbox.ParallelSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;
import tests.mazda.TestOne;

@RunWith(ParallelSuite.class)

@SuiteClasses({
        TestOne.class
})

public class PoCSuite { }
