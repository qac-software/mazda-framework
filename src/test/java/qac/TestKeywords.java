package qac;

import org.junit.Test;
import org.junit.BeforeClass;
import com.qaconsultants.pom.Page;
import com.qaconsultants.misc.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.NoSuchFrameException;

public class TestKeywords
{
    public class TempPage extends Page<TempPage>
    {
        public TempPage(WebDriver _driver)
        {
            super(_driver);
        }

        @Override
        public String getTitle()
        {
            return null;
        }

        @Override
        public String getURL()
        {
            return null;
        }
    }

    static WebDriver driver;

    @BeforeClass
    public static void before() throws Exception
    {
        System.setProperty("qac.webdriverDirectory", "./resources/webdrivers/");
        System.setProperty("qac.logDirectory", "./logs/tests/");
        driver = DriverFactory.getLocalDriver("CHROME");
        driver.navigate().to("https://www.google.ca");
    }

    @Test
    public void testSwitchFrameSuccess() throws Exception
    {
        (new TempPage(driver)).navigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_button_test")
                .switchFrame("iframeResult");
    }

    @Test(expected = NoSuchFrameException.class)
    public void testSwitchFrameException() throws Exception
    {
        (new TempPage(driver)).navigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_button_test")
                .switchFrame("NotReal");
    }

    @Test
    public void testClick() throws Exception
    {
        (new TempPage(driver)).navigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_button_test")
                .switchFrame("iframeResult")
                .clickElement(driver.findElement(By.xpath("/html/body/button")));

        try
        {
            driver.switchTo().alert().accept();
        }
        catch(Exception e)
        {
            assert(false);
        }
    }

    @Test
    public void testInput() throws Exception
    {
        (new TempPage(driver)).navigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_form_submit")
                .switchFrame("iframeResult")
                .inputText(driver.findElement(By.name("FirstName")), "Tyler")
                .inputText(driver.findElement(By.name("LastName")),  "Remazki")
                .clickElement(driver.findElement(By.xpath("/html/body/form/input[3]")))
                .waitForVisible(driver.findElement(By.xpath("/html/body/div[1]")));
        assert(driver.findElement(By.xpath("/html/body/div[1]")).getText().trim().contains("FirstName=MickeyTyler&LastName=MouseRemazki"));
    }

    @Test
    public void testTextContains() throws Exception
    {
        (new TempPage(driver)).navigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_form_submit")
                .switchFrame("iframeResult")
                .inputText(driver.findElement(By.name("FirstName")), "Tyler")
                .inputText(driver.findElement(By.name("LastName")),  "Remazki")
                .clickElement(driver.findElement(By.xpath("/html/body/form/input[3]")))
                .waitForVisible(driver.findElement(By.xpath("/html/body/div[1]")))
                .validateTextContains(driver.findElement(By.xpath("/html/body/div[1]")), "MickeyTyler");
    }

    @Test
    public void testDropdown() throws Exception
    {
        (new TempPage(driver)).navigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select")
                .switchFrame("iframeResult")
                .selectFromDropdownByValue(driver.findElement(By.xpath("/html/body/select")), "opel");
        assert(driver.findElement(By.xpath("/html/body/select")).getAttribute("value").equals("opel"));
    }

    @Test
    public void testNavigation() throws Exception
    {
        (new TempPage(driver)).navigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_select")
                .navigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_form_submit")
                .navigateBack()
                .navigateForward();
    }

    @Test
    public void testTabSwitch() throws Exception
    {
        WebDriver _driver;
        _driver = (new TempPage(driver)).navigateTo("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_link_target")
                .switchFrame("iframeResult")
                .clickElement(driver.findElement(By.xpath("/html/body/a")))
                .switchTab("2")
                .validateTitle("W3Schools Online Web Tutorials")
                .switchTab("1")
                .validateTitle("Tryit Editor v3.5")
                .getDriver();
        assert(_driver.getCurrentUrl().equals("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_link_target"));
    }
}
