package qac;

import com.qaconsultants.misc.DriverFactory;
import com.qaconsultants.pom.Page;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TestPage
{
    class MyPage extends Page<MyPage>
    {
        @FindBy(name="q")
        public WebElement searchbar;

        public MyPage(WebDriver _driver)
        {
            super(_driver);
        }

        @Override
        public String getTitle()
        {
            return "Google";
        }

        @Override
        public String getURL()
        {
            return "https://www.google.ca";
        }
    }

    static WebDriver driver;
    public MyPage mPage;

    @BeforeClass
    public static void before() throws Exception
    {
        System.setProperty("qac.webdriverDirectory", "./resources/webdrivers/");
        driver = DriverFactory.getLocalDriver("CHROME");
        driver.navigate().to("https://www.google.ca");
    }

    @Test
    public void testPageGetTitle() throws Exception
    {
        mPage = new MyPage(driver);
        assert(mPage.getTitle().equals("Google"));
    }

    @Test
    public void testPageGetURL() throws Exception
    {
        mPage = new MyPage(driver);
        assert(mPage.getURL().equals("https://www.google.ca"));
    }

    @Test
    public void testPageThroughSeleniumFactory() throws Exception
    {
        mPage = new MyPage(driver);
        assert(mPage.searchbar != null);
    }

    @Test
    public void testReturnSelfSuperclassMethods() throws Exception
    {
        mPage = new MyPage(driver);
        assert(mPage.navigate() instanceof MyPage);
    }

}
