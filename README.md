# Mazda Java Framework #

The Mazda Java Framework is a proof-of-concept for Mazda front end automation.
It was created to ensure that the Mazda Build-A-Car webpage (https://www.mazda.ca/en/shopping-tools/build-price/#/ON) could be functionally tested.

### Setup ###

* Configuration
* Deployment instructions

---

### Red Flags ###

- Unique identifiers are few and far between on the web-page and are not on major functional elements
- Losing focus of the window will typically cause Selenium to fail to do something. Unsure of what the cause is. 
- Maximizing the window is required, otherwise Selenium may click the wrong element due to overlay
    - Not an issue currently, but may be a symptom of a bigger issue in their responsive design
- As of Monday, March 12th, 2018, no data has been provided for verification purposes
    - Solution: We may provide them with the results of the scraping post-build

---

### Meeting Notes ###

Monday, March 12th, 2018

- Two slides are to be produced by Rob with my assistance in gathering information.
- Detail any further questions for Mazda 
- Answer the following questions:
    - Is it feasible to automate? 
        - Yes, it is feasible to automate from a functional perspective.  
    - What would be our approach? 
        - Java + Selenium framework utilizing a POM (page object model) to structure the test cases and external data will dictate the options that are chosen.
    - Other questions for them?   
        - Following up: Was it touched upon whether or not their development team will be able to accomodate automation with additional unique identifiers? 
        -  
- Additional Questions:
    - 

---

### Questions ###

- Will the verification data be provided to us? (NOT ANSWERED)
    - We must ensure that there is a concrete number value we are testing against that is generated from the back-end calculations (Which are assumed to be correct)
    - If no, will we be providing them with the framework results for cross-comparison?
    - If yes, we will perform data verification using the framework

- Will the development team accomodate automated testing with the addition of unique identifiers? (NOT ANSWERED)

---

### Who do I talk to? ###

- Tyler Remazki (tremazki@qaconsultants.com)